package familia;

//Importo las librerías de AWT y de SQL
import java.awt.event.*;
import java.sql.*;

public class Modelo {

    //Declaro los atributos String que utilizaré para conectarme query la base de datos
    private String driver;
    private String prefijoConexion;
    private String ip;
    private String usr;
    private String psw;
    private String bd;

    //Declaro un string 'tabla' que utilizaré para consultar query la base de datos
    private String tabla;
 
    //Declaro dos strings donde me guardo el resultado de las consultas query la base de datos
    //private String resultadoHijoConsulta;
    private String resultadoPadreConsulta;
    private int numeroHijo;
    //Declaro un atributo de tipo Connection, que representará mi referencia query la base de datos
    private Connection connection;
    //Declaro un atributo de tipo ActionListener, que representa el evento que se activará 
    //cuando ocurra una excepción
    private ActionListener listener;

    //Declaro un constructor vacío del modelo, donde inicializo algunos de sus atributos de tipo String
    public Modelo() {
        driver = "com.mysql.jdbc.Driver";
        prefijoConexion = "jdbc:mysql://";
        ip = "localhost";             // Direccion IP donde esta corriendo el SGBD
        usr = "";                     // Usuario
        psw = "";                     // Password
        bd = "familia";               // Base de datos
        tabla = "integrantes";         // Tabla de las estaciones
 
        resultadoPadreConsulta = "";
    }

    //Hago los getters para el nombre de la estación donde estoy parado y su comentario
    /*
    public String getResultadoHijoConsulta() {
        return resultadoHijoConsulta;
    }
*/
    public String getResultadoPadreConsulta() {
        return this.resultadoPadreConsulta;
    }
    public Integer getNumeroHijo() {
        return this.numeroHijo;
    }

    //Codifico el método 'consultar', que se ejecutará cada vez que clickee el boton 'avanzar'
    //Este método recibe un String 'nombreBuscado', y un String 'sentido', que utilizaré para generar
    //la consulta query la base de datos
    public void consultar(String nombreBuscado) throws SQLException {
        //Si la variable 'nombreBuscado' no está vacía, entro al if
        if (!nombreBuscado.isEmpty() && existeNombre(nombreBuscado)) {
            
            //Me guardo una referencia a la base de datos
            connection = obtenerConexion();
            System.out.println(connection);

            //Creo el statement
            Statement statement = connection.createStatement();
            //Creo el resultSet y lo inicializo en null
            ResultSet resultSet = null;
            
            //Inicializo el número de hijo en 1
            this.numeroHijo=1;
            //Declaro un flag que me indica cuando el nombre fue encontrado
            boolean encontrado=false;
            //Declaro el string donde genero la query
            String query;
            
            //Inicializo las tres variables con una cadena vacía (si no se inicializam, me tira error)
            String hermanoMayor = "";
            String hijoMayor = "";
            String hermanoMenor = "";
            do {
                System.out.println("Buscando..."); 
                
                //Genero el String que representa la consulta SQL a la base de datos
                query = "SELECT *"
                        + " FROM " + tabla
                        + " WHERE HijoMayor='" + nombreBuscado + "' or HermanoMenor='" + nombreBuscado + "'";

                System.out.println(query);
                
                //Ejecuto la query
                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);
                //Muestro el resultado de la query (esta query solo devuelve un registro
                Debug.imprimirResultadoQuery(resultSet, 3);
                

                //Cargo los tres campos que me devolvió la consulta en cada variable String
                
                while (resultSet.next()) {
                    hermanoMayor = resultSet.getString(1);
                    hijoMayor = resultSet.getString(2);
                    hermanoMenor = resultSet.getString(3);
                    System.out.println("hermanoMayor= " + hermanoMayor);
                    System.out.println("hijoMayor= " + hijoMayor);
                    System.out.println("hermanoMenor= " + hermanoMenor );
                    System.out.println("");
                }
                
                //Caso base: cuando el nombreBuscado lo encuentro en la segunda columna (HijoMayor), lo doy por encontrado
                if (hijoMayor.equals(nombreBuscado)) 
                  encontrado=true;
                else
                    this.numeroHijo++;
                //Si el nombreBuscado lo encuentro en la tercera columna (hermanoMenor), sigo iterando hasta
                //que lo encuentre en la segunda columna (hijoMayor). Vuelvo a realizar la búsqueda con el campo que me
                //devolvió la primera columna (hermanoMayor).
                nombreBuscado = hermanoMayor;
            }while (!encontrado) ;
            //Me guardo el resultado de la búsqueda en el atributo resultadoPadreConsulta
            this.resultadoPadreConsulta = hermanoMayor;

            resultSet.close();
            statement.close();
        }
        else{
            this.resultadoPadreConsulta= "Error: No se encontró la persona buscada";
            this.numeroHijo=0;
        }
    }

    public boolean existeNombre(String nombreBuscado) throws SQLException{
        System.out.println("Entro a existeNombre");
        String query= "SELECT * FROM " + tabla + " WHERE Nombre='" + nombreBuscado + "' OR HijoMayor='"
             + nombreBuscado + "' OR HermanoMenor='" + nombreBuscado + "'"   ;
        System.out.println(query);
        connection= obtenerConexion();
        Statement statement=connection.createStatement();
        ResultSet resultSet=statement.executeQuery(query);
        Debug.imprimirResultadoQuery(resultSet,3);       
        return resultSet.next();
    }
    //El modelo recibe una referencia al evento que se activará cuando ocurra una excepción
    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    //Este método se ejecutará cuando ocurra una excepción
    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    //Codifico el método 'obtenerConexion', que me devolverá una referencia query la base de datos
    //a la cual quiero acceder.
    private Connection obtenerConexion() {
        if (connection == null) {
            try {
                //Registra el driver en la máquina virtual
                Class.forName(driver);
            } catch (ClassNotFoundException ex) {
                reportException(ex.getMessage());
            }
            try {
                //Le paso la dirección de la base de datos, un nombre de usuario, una contraseña
                //y me devuelve una referencia a la base de datos
                String s = this.prefijoConexion + this.ip + "/" + this.bd;
                System.out.println(s);
                System.out.println(usr);
                System.out.println(psw);
                connection = DriverManager.getConnection(s, usr, psw);
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
            Runtime.getRuntime().addShutdownHook(new ShutDownHook());
        }
        //Devuelvo la referencia a la base de datos
        return connection;
    }

    //Cierro la conexión a la base de datos
    private class ShutDownHook extends Thread {

        @Override
        public void run() {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        }
    }
}