package familia;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Vista {

    private Modelo modelo;
    private JFrame frame;
    private JPanel panel;
    private JLabel hijo;
    private JLabel padre;
    private JLabel numHijo;
    private JTextField textFieldHijo;
    private JTextField textFieldPadre;
    private JTextField textFieldNumHijo;
    private JButton botonBuscarProgenitor;
    
    //Al constructor de la vista le paso una referencia al modelo
    public Vista(Modelo modelo) {
        this.modelo = modelo;
        //Le paso un exceptionListener al modelo
        this.modelo.addExceptionListener(new ExceptionListener());
        
        //En el constructor, hago el maquetado de la vista
        
        //Instancio los componentes de la vista
        frame = new JFrame();
        panel = new JPanel();
        textFieldHijo = new JTextField(26);
        botonBuscarProgenitor = new JButton("Buscar progenitor");
        textFieldPadre = new JTextField(26);
        textFieldNumHijo = new JTextField(4);
        hijo= new JLabel("Hijo");
        padre= new JLabel("Padre");
        numHijo= new JLabel("Numero de hijo");
        //Formateo el panel
        frame.getContentPane().setBackground(Color.CYAN);
        frame.setSize(600,100);
        
        //Agrego al panel los botones y cajas de texto
        panel.add(hijo);
        panel.add(textFieldHijo);
        
        panel.add(botonBuscarProgenitor);
        panel.add(padre);
        panel.add(textFieldPadre);
        panel.add(numHijo);
        panel.add(textFieldNumHijo);
        //Agrego el panel al frame
        frame.getContentPane().add(panel);
    }

    //Muestra el frame cargado con lo que instancié y agregué en el constructor
    public void mostrar() {
        //Pongo el título al frame
        frame.setTitle("Buscador de progenitores en base de datos genealogica");
        //Le digo al frame que cuando se cierre la ventana, salga del programa
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Formateo el tamaño del frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        //Hago visible el frame
        frame.setVisible(true);
    }

    public String getTextFieldHijo() {
        return textFieldHijo.getText();
    }

    public void setTextFieldHijo(String nombre) {
        textFieldHijo.setText(nombre);
    }

    public String getTextFieldPadre() {
        return textFieldPadre.getText();
    }

    public void setTextFieldPadre(String nombre) {
        textFieldPadre.setText(nombre);
    }
   
    public String getTextFieldNumHijo() {
        return textFieldNumHijo.getText();
    }

    public void setTextFieldNumHijo(String nombre) {
        textFieldNumHijo.setText(nombre);
    }
    
    //Le paso un ActionListener al boton buscarProgenitor
    public void addBotonBuscarProgenitor(ActionListener al) {
        botonBuscarProgenitor.addActionListener(al);
    }

    public void mostrarExcepcion(String s) {
        JOptionPane.showMessageDialog(frame, s, "Error!", JOptionPane.ERROR_MESSAGE);
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            mostrarExcepcion(event.getActionCommand());
        }
    }
}
