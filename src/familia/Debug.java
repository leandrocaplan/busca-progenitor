/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package familia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Leandro
 */
public class Debug {

    public static void imprimirListaVentana(ArrayList<String> conocidos) {
        String lista = "conocidos\n\n";

        for (String s : conocidos) {
            lista += (s + "\n");
        }

        JOptionPane.showMessageDialog(null, lista);
    }

    public static void imprimirLista(ArrayList<String> conocidos) {
        System.out.println("Lista de conocidos:");
        for (String s : conocidos) 
            System.out.println(s);
        System.out.println("");
    }
    
    public static void imprimirResultadoQuery(ResultSet resultado,int cantCampos) throws SQLException {
        System.out.println("Imprimir resultado");
        while (resultado.next()) {
            for (int i=1;i<(cantCampos+1);i++) {
                
                System.out.print(resultado.getString(i) + ",");
           
            }
            System.out.println("");
        }
        System.out.println("");
        resultado.beforeFirst();
    }
}
