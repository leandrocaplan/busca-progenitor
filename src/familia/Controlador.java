package familia;

import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controlador {

    //Declaro referencias al modelo y a la vista
    private Modelo modelo;
    private Vista vista;
    //Declaro un String que me indicará el sentido del recorrido

    //Declaro el constructor, donde le paso las referencias al modelo y a la vista
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
    }
    
    //Cuando corro el programa, este método es el primero que se ejecuta
    public void ejecutar() {
        //Llamo al método 'mostrar' de la vista.
        vista.mostrar();
        //Al boton 'avanzar' de la vista, le asocio el evento 'AvanzarListener'
        vista.addBotonBuscarProgenitor(new BuscarListener());
    }
    //Hago una clase anidada, que representa el evento asociado al boton 'avanzar'
    
    //BuscarListener es una clase que "hereda" de ActionListener (Se puede castear a ActionListener)
    private class BuscarListener implements ActionListener {

        @Override
        //Cuando pulso el boton 'buscar', ejecuto el siguiente bloque.
        public void actionPerformed(ActionEvent event) {
            try {
                //Ejecuto el método 'consultar' del modelo, y le paso
                //como parámetro el nombre del hijo que quiero buscar.
                modelo.consultar(vista.getTextFieldHijo());
            } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Escribo el resultado de la consulta en el textFieldPadre de la vista
            vista.setTextFieldPadre(modelo.getResultadoPadreConsulta());
            vista.setTextFieldNumHijo(modelo.getNumeroHijo().toString());
        }

        private void reportException(String message) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}