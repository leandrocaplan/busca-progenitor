-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2015 a las 21:54:13
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `familia`
--
CREATE DATABASE IF NOT EXISTS `familia` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `familia`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrantes`
--

CREATE TABLE IF NOT EXISTS `integrantes` (
  `Nombre` varchar(20) NOT NULL,
  `HijoMayor` varchar(20) NOT NULL,
  `HermanoMenor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `integrantes`
--

INSERT INTO `integrantes` (`Nombre`, `HijoMayor`, `HermanoMenor`) VALUES
('Isabel', 'Carlos', ''),
('Carlos', 'Guillermo', 'Ana'),
('Ana', 'Pedro', 'Andrés'),
('Andrés', 'Beatriz', 'Eduardo'),
('Eduardo', 'Luisa', ''),
('Guillermo', 'Jorge', 'Enrique'),
('Enrique', '', ''),
('Pedro', 'Savannah', 'Sara'),
('Sara', 'Mia', ''),
('Beatriz', '', 'Eugenia'),
('Eugenia', '', ''),
('Luisa', '', 'Jaime'),
('Jaime', '', ''),
('Jorge', '', 'Carlota'),
('Carlota', '', ''),
('Savannah', '', 'Isla'),
('Isla', '', ''),
('Mia', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
